class Item {
    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

class Shop {
    constructor(items = []) {
        this.items = items;
    }

    updateQuality() {
        this.items.length && this.items.map((item, index) => {
            if (item.name === "Aged Brie") {
                item.quality++;
                item.sellIn--;
                if (item.sellIn <= 0) {
                    item.quality++;
                }
            } else if (item.name === "Backstage passes to a TAFKAL80ETC concert") {
                item.quality++;
                if (item.sellIn <= 10) {
                    item.quality++;
                }
                if (item.sellIn <= 10) {
                    item.quality++;
                }
                if (item.quality > 50) {
                    item.quality = 50;
                }

            } else if (item.name === "Sulfuras, Hand of Ragnaros") {

            } else {
                item.quality--;
                item.sellIn--;
                if (item.sellIn <= 0) {
                    item.quality--;
                }
            }

        })

        return this.items;
    }
}

module.exports = {
    Item,
    Shop
}
