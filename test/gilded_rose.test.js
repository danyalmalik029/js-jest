const {Shop, Item} = require("../src/gilded_rose");

describe("Gilded Rose", function() {
  it("should foo", function() {
    const gildedRose = new Shop([new Item("foo",-1,0),
      new Item("Aged Brie",0,1),
      new Item("Backstage passes to a TAFKAL80ETC concert",6,49),
          new Item("Sulfuras, Hand of Ragnaros",11,50)]);
    const items = gildedRose.updateQuality();
    expect(items);
  });
});
